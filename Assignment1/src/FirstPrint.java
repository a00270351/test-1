package Assignment1.src;
import java.util.Scanner;

public class FirstPrint {

    public static void main(String[] args) {
        System.out.println("Please enter a word");
        Scanner sc = new Scanner(System.in);
        String word = sc.next();

        processWord(word);
    }
// did not recorded tutorial for you. so imshowing how to commit and push.

    public static void processWord(String word) {

        System.out.println("Word Length: " + word.length());
        System.out.println("First Letter: " + word.charAt(0));
        System.out.println("Last Letter: " + word.charAt(word.length()-1));

        processVowels(word);

    }

    public static void processVowels(String word) {

        int count = 0; // comment here

        word = word.toLowerCase();

        for (char c : word.toCharArray())
        {
            if (c =='a' || c =='e' || c =='i' || c =='o' || c =='u') // check for vowels
            {
                count++;
            }
        }

        System.out.print("Number of Vowels: " + count);

    }
}
